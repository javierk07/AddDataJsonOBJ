package com.example.javier.adddatajsonobj;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.javier.adddatajsonobj.adapter.CustomListNotifyAdapter;
import com.example.javier.adddatajsonobj.model.Notify;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_add, btn_clear;
    private EditText titulo, mensaje;
    private int numero = 0;
    JSONArray jsonArray;
    String JsonArray;
    String weekDay;
    String mes;
    int dia;
    Calendar c;

    private List<Notify> notifyList = new ArrayList<Notify>();
    private ListView listView;
    private CustomListNotifyAdapter adapter;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String js;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_add = (Button) findViewById(R.id.btn_add);
        btn_clear = (Button) findViewById(R.id.bt_clear);
        titulo = (EditText) findViewById(R.id.edt_titulo);
        mensaje = (EditText) findViewById(R.id.edt_sms);
        btn_clear.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        jsonArray = new JSONArray();


        listView = (ListView) findViewById(R.id.listView);


        prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        editor = prefs.edit();


        adapter = new CustomListNotifyAdapter(this, notifyList);
        listView.setAdapter(adapter);

        PopulateListv();
        getDia();

    }


    public String makJsonObject(String titulo, String cuerpo, String fecha, String hora)
            throws JSONException {
        JSONObject obj;

            obj = new JSONObject();
            try {
                obj.put("titulo", titulo);
                obj.put("cuerpo", cuerpo);
                obj.put("fecha", fecha);
                obj.put("hora", hora);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        JsonArray = jsonArray.toString();
        editor.putString("notifyH", JsonArray);
        editor.commit();

        return JsonArray;
    }

    public String getDia() {

        c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        int month = c.get(Calendar.MONTH);

        if (Calendar.MONDAY == dayOfWeek) weekDay = "Lunes";
        else if (Calendar.TUESDAY == dayOfWeek) weekDay = "Martes";
        else if (Calendar.WEDNESDAY == dayOfWeek) weekDay = "Miercoles";
        else if (Calendar.THURSDAY == dayOfWeek) weekDay = "Jueves";
        else if (Calendar.FRIDAY == dayOfWeek) weekDay = "Viernes";
        else if (Calendar.SATURDAY == dayOfWeek) weekDay = "Sabado";
        else if (Calendar.SUNDAY == dayOfWeek) weekDay = "Domingo";

        if (month == 0) mes = "Enero";
        else if (month == 1) mes = "Febrero";
        else if (month == 2) mes = "Marzo";
        else if (month == 3) mes = "Abril";
        else if (month == 4) mes = "Mayo";
        else if (month == 5) mes = "Junio";
        else if (month == 6) mes = "Julio";
        else if (month == 7) mes = "Agosto";
        else if (month == 8) mes = "Septiembre";
        else if (month == 9) mes = "Octubre";
        else if (month == 10) mes = "Noviembre";
        else if (month == 11) mes = "Diciembre";

        System.out.println(weekDay);
        System.out.println(mes);
        System.out.println(c.get(Calendar.DAY_OF_MONTH));

        return weekDay;
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_add){
            String hora = (String) DateFormat.format("hh:mm aaa", Calendar.getInstance().getTime());
            dia = c.get(Calendar.DAY_OF_MONTH);
            numero ++;
            try {
                makJsonObject(titulo.getText().toString(), mensaje.getText().toString(), dia+" de "+mes, hora);
                Log.d("JsonArray Final "+String.valueOf(numero), JsonArray);
                notifyList.clear();
                PopulateListv();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            titulo.setText("");
            mensaje.setText("");
        }

        if(v.getId() == R.id.bt_clear){
            editor.putString("notifyH", null);
            editor.commit();
            jsonArray = new JSONArray();
            notifyList.clear();
            adapter.notifyDataSetChanged();
        }
    }

    public void PopulateListv(){
        js = prefs.getString("notifyH", "Notify");

        try {
            jsonArray = new JSONArray(js);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            // Parsing json
            for (int i = 0; i < jsonArray.length(); i++) {
                try {

                    JSONObject obj = jsonArray.getJSONObject(i);
                    Notify notify = new Notify();
                    notify.setTitulo(obj.getString("titulo"));
                    notify.setTexto(obj.getString("cuerpo"));
                    notify.setFecha(obj.getString("fecha"));
                    notify.setHora(obj.getString("hora"));
                    notifyList.add(0,notify);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            // notifying list adapter about data changes
            // so that it renders the list view with updated data
            adapter.notifyDataSetChanged();
        }

        catch(Exception e){
            System.out.print(e+"=Exception Reason!");
        }
        finally{
            //Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show();
        }
    }

}
