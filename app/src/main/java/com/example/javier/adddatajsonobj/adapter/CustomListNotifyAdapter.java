package com.example.javier.adddatajsonobj.adapter;

/**
 * Created by javier on 02-03-17.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.javier.adddatajsonobj.model.Notify;
import com.squareup.picasso.Picasso;

import com.example.javier.adddatajsonobj.R;


import java.util.List;

public class CustomListNotifyAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Notify> notifyItems;


    public CustomListNotifyAdapter(Activity activity, List<Notify> notifyItems) {
        this.activity = activity;
        this.notifyItems = notifyItems;
    }

    @Override
    public int getCount() {
        return notifyItems.size();
    }

    @Override
    public Object getItem(int location) {
        return notifyItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_notify, null);

        //ImageView IMG_Articulo = (ImageView) convertView.findViewById(R.id.imgn_notify);


        TextView Titulo = (TextView) convertView.findViewById(R.id.txtn_titulo);
        TextView Fecha = (TextView) convertView.findViewById(R.id.txtn_fecha);
        TextView Cuerpo = (TextView) convertView.findViewById(R.id.txtn_texto);
        TextView Hora = (TextView) convertView.findViewById(R.id.txtn_hora);


        // getting movie data for the row
        Notify ntfy = notifyItems.get(position);

        // thumbnail image logos equipos
        //Picasso.with(convertView.getContext()).load(ntfy.getImagen()).into(IMG_Articulo);
        //TXT_img.setText(art.getImagen());

        // Titulo
        Titulo.setText(ntfy.getTitulo());

        //FEcha y hora
        Fecha.setText(ntfy.getFecha());
        Hora.setText(ntfy.getHora());

        // Titulo
        Cuerpo.setText(ntfy.getTexto());

        return convertView;
    }

}